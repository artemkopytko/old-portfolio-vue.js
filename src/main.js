import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import { firebaseApp } from './firebaseApp.js'
import { routes } from './routes.js'
import { store } from './store/store.js'
import { isAuthLink } from './currLink.js'
var user = firebaseApp.auth().currentUser;


Vue.use(VueRouter);
Vue.use(VueResource);
Vue.http.options.root = 'https://artemkopytko-d498f.firebaseio.com/';

const router = new VueRouter({
    routes: routes,
    mode: 'history',
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        }
        if (to.hash) {
            return {
                selector: to.hash
            }
        }
        return { x: 0, y: 0 }
    }
});


new Vue({
    el: '#app',
    router: router,
    store: store,
    render: h => h(App)
});
