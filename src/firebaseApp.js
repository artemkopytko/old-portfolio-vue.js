import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyDoM_bsnPeD61VNBQn8hwkmtGuhPfGSqa0",
    authDomain: "artemkopytko-d498f.firebaseapp.com",
    databaseURL: "https://artemkopytko-d498f.firebaseio.com",
    projectId: "artemkopytko-d498f",
    storageBucket: "artemkopytko-d498f.appspot.com",
    messagingSenderId: "178475818401"
};

export const firebaseApp = firebase.initializeApp(config);
export const eventsRef = firebase.database().ref().child('events');