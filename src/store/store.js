/**
 * Created by artemkopytko on 08.05.17.
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        enquireFormStatus: false,
        pageIsLoaded: false,
        user: {},
        userIsLoggedIn: false
    },
    getters: {
        enquireFormStatus: state => {
            return state.enquireFormStatus;
        },
        user: state => {
            return state.user;
        }
    },
    mutations: {
        revealEnquireForm: state => {
            state.enquireFormStatus = true;
        },
        closeEnquireForm: state => {
            state.enquireFormStatus = false;
        },
        pageWasLoaded: state => {
            state.pageIsLoaded = true;
        },
        loggedIn: (state,payload) => {
            state.user = payload;
            state.userIsLoggedIn = true;
        },
        signOut: state => {
            state.user = {};
            state.userIsLoggedIn = false;
        }
    },
    actions: {
        loggedIn: ({commit}, payload) => {
            commit('loggedIn',payload)
        },
        signOut: ({commit}) => {
            commit('signOut');
        }
    }

});