/**
 * Created by artemkopytko on 03.05.17.
 */
import $ from 'jquery'
export const valEmail = function validateEmail(emailVal) {
    const emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    const emailValue = emailVal;
    const errorBoxMail = $('.enquire-err-mail');
    if (emailValue === '') {
        errorBoxMail.text('email cannot be blank');
        return false;
    } else if (!(emailValue).match(emailReg)) {
        errorBoxMail.text('not an email');
        return false;
    } else {
        return true;
    }
};