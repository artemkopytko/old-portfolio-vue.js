/**
 * Created by artemkopytko on 12.03.17.
 */
import $ from './jquery-3.2.0.min.js'

export const smoothScroll =
$(document).ready(function () {
    $(function() {
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top -70
                    }, 1000);
                    return false;
                }
            }
        });
    });
});