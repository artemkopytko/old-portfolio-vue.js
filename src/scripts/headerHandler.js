/**
 * Created by artemkopytko on 05.04.17.
 */
import $ from './jquery-3.2.0.min.js'

export const scrollFunc = $(document).ready(function () {
$(window).scroll(function () {
    if($(window).scrollTop() > 0) {
        $('.header').addClass('header-shadow');
    } else {
        $('.header').removeClass('header-shadow');
    }
})
});


