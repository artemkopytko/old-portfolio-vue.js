/**
 * Created by artemkopytko on 03.05.17.
 */
import $ from 'jquery'
export const valComment = function validateComment(commentVal) {
    const errorBoxComment = $('.enquire-err-msg');
    if (commentVal === '') {
        errorBoxComment.text('comment cannot be blank');
        return false;
    } else {
        return true;
    }
};