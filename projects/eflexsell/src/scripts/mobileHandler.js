/**
 * Created by artemkopytko on 28.05.17.
 */
$(document).ready(function(){
    var hdIsOpened = false;
    $('#nav-icon3').click(function(){
        $(this).toggleClass('open');
        $('.header-content-mobile-side').css('display','block');
    });
    $('.sideHideButton').click(function () {
        $('#nav-icon3').toggleClass('open');
        $('.header-content-mobile-side').css('display','none');
    })
    $('.help-dropdown-toggler').click(function () {
        if(!hdIsOpened) {
            $('.help-dropdown').css('display','block');
            $('a.help-dropdown-toggler').css('color','ff5959');
            hdIsOpened = true;
        } else {
            $('.help-dropdown').css('display','none');
            $('a.help-dropdown-toggler').css('color','4b4b4b');
            hdIsOpened = false;
        }
    })
});